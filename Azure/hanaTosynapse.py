from .libraries import *



class SapHanaToAzureDataFactoryToSynapse:
    def __init__(self, config):
        self._rg_name = "practice_kanik "  #config["Azure"]["ResourceGroup"]
        self._rg_params = "eastus" #config["Azure"]["ResourceGroup"]["location"]
        self._df_name = "dataFactoryKanik" #config["Azure"]["DataFactory"]
        self._client_id = config["Azure"]["ServicePrincipal"]["ClientID"]
        self._client_secret = config["Azure"]["ServicePrincipal"]["ClientSecret"]
        self._tenant_id = config["Azure"]["ServicePrincipal"]["TenantID"]
        self._subscription_id = "474d88f9-1e55-41a1-808b-81724edda1ca" #config["Azure"]["ServicePrincipal"]["SubscriptionID"]
        self._df_client = DataFactoryManagementClient(
            credentials=ServicePrincipalCredentials(
                client_id=self._client_id,
                secret=self._client_secret,
                tenant=self._tenant_id,
            ),
            subscription_id=self._subscription_id,
        )

        self._sh_client = HanaManagementClient(
            credentials=ServicePrincipalCredentials(
                client_id=self._client_id,
                secret=self._client_secret,
                tenant=self._tenant_id,
            ),
            subscription_id=self._subscription_id,
        )

    def _create_data_factory(self):
        """
            This Function execute only when you have to create data factory only
        """
        df_resource = Factory(location=self._rg_params)
        data_factory = self._df_client.factories.create_or_update(self._rg_name, self._df_name, df_resource)
        while data_factory.provisioning_state != 'Succeeded':
            data_factory = self._df_client.factories.get(self._rg_name, self._df_name)
            time.sleep(1)

    """ Hana to azure linked services"""
    def _create_hana_linked_services(self):
        _hana_ls_name = 'SapHanaLinkedService'
        _hana_client_id = ""
        _hana_user_name = ""
        _hana_server = ""
        _hana_system_number = ""
        _hana_id_password = ""
        _hana_connection_string = ""
        _hana_authentication_type = ""
        _connect_via = IntegrationRuntimeReference(reference_name=_hana_ls_name)
        _hana_ls = SapHanaLinkedService(
            connect_via=_connect_via,
            client_id=_hana_client_id,
            user_name=_hana_user_name,
            server=_hana_server,
            system_number=_hana_system_number,
            connection_string=_hana_connection_string,
            authentication_type=_hana_authentication_type,
            password=SecureString(value=_hana_id_password),
        )
        _sap_ls_out = self._df_client.linked_services.create_or_update(
            self._rg_name, self._df_name, _hana_ls_name,_hana_ls
        )
        return LinkedServiceReference(reference_name=_hana_ls_name)


    def _create_hana_dataset_input(self):  # input
        _hana_table_name = ""
        _hana_dataset_name = "SAPHANADataset"
        _hana_ls = self._create_hana_linked_services()
        _hana_dataset = SapHanaTableDataset(
            linked_service_name=_hana_ls,
            table_name=_hana_table_name
        )
        dataset = self._df_client.datasets.create_or_update(
            self._rg_name, self._df_name, _hana_dataset_name, _hana_dataset
        )
        return DatasetReference(reference_name=_hana_dataset_name)

    def _create_azure_synapse_analytic_linked_services(self):                                                          
        """
            Azure Data Factory to Azure Synapse Analytic
            Link : https://docs.microsoft.com/en-us/azure/data-factory/connector-azure-sql-data-warehouse
        :return:
        """
        _synapse_ls_name = "AzureSqlDWLinkedService"
        _synapse_type = "Azure_Sql_DW"
        _synapse_connection_string = ""
        _synapse_service_principal_id = ""
        _synapse_service_principal_key =""
        _synapse_tenant = ""
        _connect_via = IntegrationRuntimeReference(reference_name=_synapse_ls_name)
        _synapse_ls = AzureSqlDWLinkedService(
            connect_via=_connect_via,
            connection_string=_synapse_connection_string,
            service_principal_id=_synapse_service_principal_id,
            service_principal_key=_synapse_service_principal_key,
            type=_synapse_type,
            tenant=_synapse_tenant,
        )
        return LinkedServiceReference(reference_name=_synapse_ls_name)

    def _create_azure_synapse_analytic_dataset_output(self):  # output
        _synapse_dataset_name = "AzureSQLDWDataset"
        _synapse_table_name = ""
        _synapse_type = ""
        _synapse_ls = self._create_azure_synapse_analytic_linked_services()
        _synapse_dataset = AzureSqlDWTableDataset(
            linked_service_name=_synapse_ls,
            type=_synapse_type,
            table_name=_synapse_table_name
        )
        dataset = self._df_client.datasets.create_or_update(
            self._rg_name, self._df_name, _synapse_dataset_name, _synapse_dataset
        )
        return DatasetReference(reference_name=_synapse_dataset_name)

    def _create_copy_activity(self):
        _activity_name = "Copy From SAP HANA To Azure Synapse Analytic"
        _hana_ds = self._create_hana_dataset_input()
        _synapse_ds = self._create_azure_synapse_analytic_dataset_output()
        return CopyActivity(
            name=_activity_name,
            inputs=[_hana_ds],
            outputs=[_synapse_ds],
            sourse=SapHanaSource(),
            sink=SqlDWSink(),
        )

    def _create_pipeline(self):
        _pipeline_name = "Hana_Load_Pipeline"
        _copy_activity = self._create_copy_activity()
        _pipeline = PipelineResource(activities=[_copy_activity])
        _pipeline_out = self._df_client.pipelines.create_or_update(
            self._rg_name,
            self._df_name,
            _pipeline_name,
            _pipeline
            )
        return PipelineReference(reference_name=_pipeline_name)

    def run_pipeline(self):
        run_response = self._df_client.pipelines.create_run(
            self._rg_name,
            self._df_name,
            f"{'Hana_Load_Pipeline'}",
            reference_pipeline_run_id=self._create_pipeline(),
            parameters={}
        )
        return run_response


