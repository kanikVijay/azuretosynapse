from .libraries import *



class AmazonRedshiftToAzureDataFactoryToSynapse:
    def __init__(self, config):
        self._rg_name = config["Azure"]["ResourceGroup"]
        self._rg_params = config["Azure"]["ResourceGroup"]["location"]
        self._df_name = config["Azure"]["DataFactory"]
        self._client_id = config["Azure"]["ServicePrincipal"]["ClientID"]
        self._client_secret = config["Azure"]["ServicePrincipal"]["ClientSecret"]
        self._tenant_id = config["Azure"]["ServicePrincipal"]["TenantID"]
        self._subscription_id = config["Azure"]["ServicePrincipal"]["SubscriptionID"]
        self._df_client = DataFactoryManagementClient(
            credentials=ServicePrincipalCredentials(
                client_id=self._client_id,
                secret=self._client_secret,
                tenant=self._tenant_id,
            ),
            subscription_id=self._subscription_id,
        )
        self._rs_client = ResourceManagementClient(
            credentials=ServicePrincipalCredentials(
                client_id=self._client_id,
                secret=self._client_secret,
                tenant=self._tenant_id,
            ),
            subscription_id=self._subscription_id,
        )
        self._sh_client = HanaManagementClient(
            credentials=ServicePrincipalCredentials(
                client_id=self._client_id,
                secret=self._client_secret,
                tenant=self._tenant_id,
            ),
            subscription_id=self._subscription_id,
        )

    def _create_resource_group(self):
        """
            This function execute only when you have to create resource group only
        """
        self._rs_client.resource_groups.create_or_update(self._rg_name, self._rg_params)


    def _create_data_factory(self):
        df_resource = Factory(location=self._rg_params)
        data_factory = self._df_client.factories.create_or_update(self._rg_name, self._df_name, df_resource)
        while data_factory.provisioning_state != 'Succeeded':
            data_factory = self._df_client.factories.get(self._rg_name, self._df_name)
            time.sleep(1)

    """ Hana to azure linked services"""
    def _create_redshift_linked_services(self):
        _redshift_ls_name = 'AmazonRedshiftLinkedService'
        _redshift_client_id = ""
        _redshift_user_name = ""
        _redshift_server = ""
        _redshift_system_number = ""
        _redshift_id_password = ""
        _redshift_connection_string = ""
        _redshift_authentication_type = ""
        _connect_via = IntegrationRuntimeReference(reference_name=_redshift_ls_name)
        _redshift_ls = AmazonRedshiftLinkedService(
            connect_via=_connect_via,
            client_id=_redshift_client_id,
            user_name=_redshift_user_name,
            server=_redshift_server,
            system_number=_redshift_system_number,
            connection_string=_redshift_connection_string,
            authentication_type=_redshift_authentication_type,
            password=SecureString(value=_redshift_id_password),
        )
        _sap_ls_out = self._df_client.linked_services.create_or_update(
            self._rg_name,
            self._df_name,
            _redshift_ls_name,
            _redshift_ls
        )
        return LinkedServiceReference(reference_name=_redshift_ls_name)


    """
        Copy data from Amazon RedShift to Amazon S3 and the to Azure to synapse.
    """
    def _create_redshift_dataset_input(self):  # input
        _redshift_table_name = ""
        _redshift_dataset_name = "AmazonRedshiftDataset"
        _redshift_ls = self._create_redshift_linked_services()
        _redshift_dataset = AmazonRedshiftTableDataset(
            linked_service_name=_redshift_ls,
            table_name=_redshift_table_name
        )
        dataset = self._df_client.datasets.create_or_update(
            self._rg_name,
            self._df_name,
            _redshift_dataset_name,
            _redshift_dataset
        )
        return DatasetReference(reference_name=_redshift_dataset_name)

    """
        Azure Data Factory to Azure Synapse Analytic
        Link : https://docs.microsoft.com/en-us/azure/data-factory/connector-azure-sql-data-warehouse
    """
    def _create_azure_synapse_analytic_linked_services(self):
        _synapse_ls_name = "AzureSqlDWLinkedService"
        _synapse_type = "Azure_Sql_DW"
        _synapse_connection_string = ""
        _synapse_service_principal_id = ""
        _synapse_service_principal_key = ""
        _synapse_tenant = ""
        _connect_via = IntegrationRuntimeReference(reference_name=_synapse_ls_name)
        _synapse_ls = AzureSqlDWLinkedService(
            connect_via=_connect_via,
            connection_string=_synapse_connection_string,
            service_principal_id=_synapse_service_principal_id,
            service_principal_key=_synapse_service_principal_key,
            type=_synapse_type,
            tenant=_synapse_tenant,
        )
        return LinkedServiceReference(reference_name=_synapse_ls_name)

    def _create_azure_synapse_analytic_dataset_output(self):  # output
        _synapse_dataset_name = "AzureSQLDWDataset"
        _synapse_table_name = ""
        _synapse_type = ""
        _synapse_ls = self._create_azure_synapse_analytic_linked_services()
        _synapse_dataset = AzureSqlDWTableDataset(
            linked_service_name=_synapse_ls,
            type=_synapse_type,
            table_name=_synapse_table_name
        )
        dataset = self._df_client.datasets.create_or_update(
            self._rg_name, self._df_name, _synapse_dataset_name, _synapse_dataset
        )
        return DatasetReference(reference_name=_synapse_dataset_name)

    def _create_s3_linked_service_name(self):
        _s3_ls_name = "AmazonS3LinkedService"
        _connect_via = IntegrationRuntimeReference(reference_name=_s3_ls_name)
        _s3_ls = AmazonS3LinkedService(
            connect_via=_connect_via
        )

    def _create_copy_activity(self):
        _activity_name = "Copy From Amazon RedShift To Azure Synapse Analytic"
        _redshift_ds = self._create_redshift_dataset_input()
        _redshift_s3_bucket_name = ""
        _s3_linked_service_name = self._create_s3_linked_service_name()
        _redshift_unload = RedshiftUnloadSettings(
            s3_linked_service_name=_s3_linked_service_name,
            bucket_name=_redshift_s3_bucket_name
        )
        _synapse_ds = self._create_azure_synapse_analytic_dataset_output()
        return CopyActivity(
            name=_activity_name,
            inputs=[_redshift_ds],
            outputs=[_synapse_ds],
            sourse=AmazonRedshiftSource(
                redshift_unload_settings=_redshift_unload
            ),
            sink=SqlDWSink(),
        )

    def _create_pipeline(self):
        _pipeline_name = "RedShift_Load_Pipeline"
        _copy_activity = self._create_copy_activity()
        _pipeline = PipelineResource(activities=[_copy_activity])
        _pipeline_out = self._df_client.pipelines.create_or_update(
            self._rg_name,
            self._df_name,
            _pipeline_name,
            _pipeline
            )
        return PipelineReference(reference_name=_pipeline_name)

    def run_pipeline(self):
        run_response = self._df_client.pipelines.create_run(
            self._rg_name,
            self._df_name,
            f"{'RedShift_Load_Pipeline'}",
            reference_pipeline_run_id=self._create_pipeline(),
            parameters={}
        )
        return run_response


