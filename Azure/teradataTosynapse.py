from .libraries import *


class TeradataToAzureDataFactoryToSynapse:
    def __init__(self, config):
        self._rg_name = config["Azure"]["ResourceGroup"]
        self._rg_params = config["Azure"]["ResourceGroup"]["location"]
        self._df_name = config["Azure"]["DataFactory"]
        self._client_id = config["Azure"]["ServicePrincipal"]["ClientID"]
        self._client_secret = config["Azure"]["ServicePrincipal"]["ClientSecret"]
        self._tenant_id = config["Azure"]["ServicePrincipal"]["TenantID"]
        self._subscription_id = config["Azure"]["ServicePrincipal"]["SubscriptionID"]
        self._df_client = DataFactoryManagementClient(
            credentials=ServicePrincipalCredentials(
                client_id=self._client_id,
                secret=self._client_secret,
                tenant=self._tenant_id,
            ),
            subscription_id=self._subscription_id,
        )
        self._rs_client = ResourceManagementClient(
            credentials=ServicePrincipalCredentials(
                client_id=self._client_id,
                secret=self._client_secret,
                tenant=self._tenant_id,
            ),
            subscription_id=self._subscription_id,
        )

    def _create_resource_group(self):
        """
            This function execute only when you have to create resource group only
        """
        self._rs_client.resource_groups.create_or_update(self._rg_name, self._rg_params)

    def _create_data_factory(self):
        df_resource = Factory(location=self._rg_params)
        data_factory = self._df_client.factories.create_or_update(self._rg_name, self._df_name, df_resource)
        while data_factory.provisioning_state != 'Succeeded':
            data_factory = self._df_client.factories.get(self._rg_name, self._df_name)
            time.sleep(1)

    def _create_teradata_linked_services(self):
        _teradata_ls_name = "OnPremTeradataLinkedService"
        _teradata_client_id = ""
        _teradata_user_name = ""
        _teradata_server = ""
        _teradata_system_number = ""
        _teradata_connection_string = ""
        _teradata_authentication_type = ""  # basic, window
        _teradata_id_password = ""
        _connect_via = IntegrationRuntimeReference(reference_name=_teradata_ls_name)
        _teradata_ls = TeradataLinkedService(
            connect_via=_connect_via,
            client_id=_teradata_client_id,
            user_name=_teradata_user_name,
            server=_teradata_server,
            system_number=_teradata_system_number,
            connection_string=_teradata_connection_string,
            authentication_type=_teradata_authentication_type,
            password=SecureString(value=_teradata_id_password),
        )

        _teradata_ls_out = self._df_client.linked_services.create_or_update(
            self._rg_name, self._df_name, _teradata_ls_name, _teradata_ls
        )
        return LinkedServiceReference(reference_name=_teradata_ls_name)

    def _create_teradata_dataset_input(self):
        _teradata_table_type = ""  # TeradataTable or RelationalTable
        _teradata_dataset_name = "TeradataDataset"
        _teradata_ls = self._create_teradata_linked_services()
        _teradata_database_name = ""
        _teradata_table_name = ""
        _teradata_dataset = TeradataTableDataset(
            linked_service_name=_teradata_ls,
            database=_teradata_database_name,
            table=_teradata_table_name,
            type=_teradata_table_type,
        )
        dataset = self._df_client.datasets.create_or_update(
            self._rg_name, self._df_name, _teradata_dataset_name, _teradata_dataset
        )
        return DatasetReference(reference_name=_teradata_dataset_name)

    def _create_azure_synapse_analytic_linked_services(self):
        _synapse_ls_name = "AzureSqlDWLinkedService"
        _synapse_type = "Azure_Sql_DW"
        _synapse_connection_string = ""
        _synapse_service_principal_id = ""
        _synapse_service_principal_key = ""
        _synapse_tenant = ""
        _connect_via = IntegrationRuntimeReference(reference_name=_synapse_ls_name)
        _synapse_ls = AzureSqlDWLinkedService(
            connect_via=_connect_via,
            connection_string=_synapse_connection_string,
            service_principal_id=_synapse_service_principal_id,
            service_principal_key=_synapse_service_principal_key,
            type=_synapse_type,
            tenant=_synapse_tenant,
        )
        return LinkedServiceReference(reference_name=_synapse_ls_name)

    def _create_azure_synapse_analytic_dataset_output(self):  # output
        _synapse_dataset_name = "AzureSQLDWDataset"
        _synapse_table_name = ""
        _synapse_type = ""
        _synapse_ls = self._create_azure_synapse_analytic_linked_services()
        _synapse_dataset = AzureSqlDWTableDataset(
            linked_service_name=_synapse_ls,
            type=_synapse_type,
            table_name=_synapse_table_name
        )
        dataset = self._df_client.datasets.create_or_update(
            self._rg_name, self._df_name, _synapse_dataset_name, _synapse_dataset
        )
        return DatasetReference(reference_name=_synapse_dataset_name)

    def _create_copy_activity(self):
        _activity_name = "Copy From Teradata To Azure Synapse Analytic"
        _teradata_ds = self._create_teradata_dataset_input()
        _synapse_ds = self._create_azure_synapse_analytic_dataset_output()
        return CopyActivity(
            name=_activity_name,
            inputs=[_teradata_ds],
            outputs=[_synapse_ds],
            sourse=TeradataSource(),
            sink=SqlDWSink(),
        )

    def _create_pipeline(self):
        _pipeline_name = "Teradata_Load_Pipeline"
        _copy_activity = self._create_copy_activity()
        _pipeline = PipelineResource(activities=[_copy_activity])
        _pipeline_out = self._df_client.pipelines.create_or_update(
            self._rg_name,
            self._df_name,
            _pipeline_name,
            _pipeline
            )
        return PipelineReference(reference_name=_pipeline_name)

    def run_pipeline(self):
        run_response = self._df_client.pipelines.create_run(
            self._rg_name,
            self._df_name,
            f"{'Teradata_Load_Pipeline'}",
            reference_pipeline_run_id=self._create_pipeline(),
            parameters={}
        )
        return run_response




